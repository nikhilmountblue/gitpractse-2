const pathfile1='../data/deliveries.csv'
const csv1=require('csvtojson');
const pathfile='../data/matches.csv'
const csv=require('csvtojson')
const fs=require('fs');
csv()
.fromFile(pathfile)
.then((j)=>{deliver(j)});
csv1()
.fromFile(pathfile1)
.then((j1)=>{match(j1)});


let Extra_Runs_Year=2016
let Top_10_Bowler_year=2015


const deliver=(j)=>{IPL_Match_Played_Per_Year(j)
    Matches_won_by_each_team_per_Year(j)
    return j}
const match=(j)=>{ Extra_Runs_Conceded_by_each_team_in(j)
                   Top_10_Economical_Bowler(j)
        return j}
  
        
// Formating the Output As Readable 
        function output(folder_name,ans)
        {
            let str=JSON.stringify(ans)
            let ansstr=''
            for(let index=0;index<str.length;index++)
            {
                if(str[index]===',')
                ansstr+=', \n '
            else if(str[index]==='{')
                ansstr+='\n { \n'
            else if(str[index]==='}')
            ansstr+='\n }'
            else
                ansstr+=str[index]
            }
             fs.writeFile('../output/'+folder_name+'.json', ansstr, function (err) {
                 if (err) return console.log("An Error Occured while writing a file")});
        }



//The bellow Function is codded to Extract Number of Ipl Match Played Per Year And Even to Extract Essential Data for other function 
let start_Extra=0
let end_Extra=0
let start_Bowler=0
let end_Bowler=0
function IPL_Match_Played_Per_Year(j)
{
    let ans={}
    let x=0
    let y=0
    let z=0
    let num=0
    for(var i=1;i<j.length;i++)
    {
        if(j[x].season!==j[i].season)
        {
            ans[j[x].season]=i-num
            x=i
            num=i
        }
        if(j[i].season===Extra_Runs_Year+'')
        {
            if(y===0)
                start_Extra=i+1;
            end_Extra=i+1
            y+=1
        }
        if(j[i].season===Top_10_Bowler_year+'')
        {
            if(z===0)
                start_Bowler=i+1;
            end_Bowler=i+1
            z+=1
        }        
    }
    ans[j[j.length-1].season]=j.length-num
    output('IPL_Match_Played_Per_Year',ans)
}


//Bellow Function is codded to Extract Matches_won_by_each_team_per_Year 
 function Matches_won_by_each_team_per_Year(j)
{
    
    let ans={}
    let x=1
    let num=0
    ans[j[1].season]={}
    ans[j[1].season][j[1].winner]=1
    for(var i=1;i<j.length;i++)
    {
        if(j[x].season!==j[i].season)
        {
            ans[j[i].season]={}
            ans[j[i].season][j[i].winner]=1
            x=i
        }
        else
        {
            if(!j[i].winner)
                j[i].winner=' '+'Draw'
            if(ans[j[i].season][j[i].winner]===undefined)
                ans[j[i].season][j[i].winner]=1
            else
                ans[j[i].season][j[i].winner]+=1
            
        }
    }

    output('Matches_won_by_each_team_per_Year',ans)
}



//Bellow function is used to Extract Extra Runs Conceded by each team in particular year
function Extra_Runs_Conceded_by_each_team_in(j1)
{
    let ans={}
    let an=Object.values(j1)
    const finns=(element)=>{
        
        if(parseInt(element.match_id)>=start_Extra&&parseInt(element.match_id)<=end_Extra)
        {
            if(ans[element.bowling_team]!==undefined)
            {ans[element.bowling_team]+=parseInt(element.extra_runs)}
            else
            {ans[element.bowling_team]=parseInt(element.extra_runs)}
        }
    }
    an.find(finns)
    output('Extra_Runs_Coceded_by_each_team_in_'+Extra_Runs_Year+'',ans)
}
//Bellow function is used to Extract Top 10 Economical Bowlers in particular Year
function Top_10_Economical_Bowler(j1)
{   
    let ans={}
    for(let index=0;index<j1.length;index++)
    {

        if(parseInt(j1[index].match_id)>=start_Bowler&&parseInt(j1[index].match_id)<=end_Bowler)
        {
            if(ans[j1[index].bowler]===undefined)
            {
                if(parseInt(j1[index].wide_runs)===0||parseInt(j1[index].noball_runs)===0)
                {
                        ans[j1[index].bowler]={
                        no_of_balls:1,
                        score:score_count(j1[index].total_runs)}
                }
                else
                {
                        ans[j1[index].bowler]={
                        no_of_balls:0,
                        score:score_count(j1[index].total_runs)}
               }
            }
            else 
            {

                if(parseInt(j1[index].wide_runs)===0||parseInt(j1[index].noball_runs==0))
                {
                    ans[j1[index].bowler].no_of_balls+=1
                    ans[j1[index].bowler].score+=score_count(j1[index].total_runs)
                }
                else
                {
                    ans[j1[index].bowler].no_of_balls+=0
                    ans[j1[index].bowler].score+=score_count(j1[index].total_runs)
                }
            }
        }
    }
    let all={}
    let top10={}
    for(let key in ans)
    all[key]=(ans[key].score/(ans[key].no_of_balls))*6

    let key=Object.keys(all)
    key.sort(function(a, b) { return all[a] - all[b] });


    for(let index=0;index<10;index++)
    top10[key[index]]=all[key[index]]
    output('Top_10_Economical_Bowler_'+Top_10_Bowler_year+'',top10)
}

const score_count=(element)=>{
    if(parseInt(element)) return parseInt(element);
    else return 0;
}


